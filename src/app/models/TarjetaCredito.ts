//41.
export class TarjetaCredito {
  id?:string;
  titular:string
  numeroTarjeta:string;
  fechaExpiracion:string;
  clave:number;

  constructor (titular:string,numeroTarjeta:string,fechaExpiracion:string,clave:number){
    this.titular=titular;
    this.numeroTarjeta=numeroTarjeta;
    this.fechaExpiracion=fechaExpiracion;
    this.clave=clave;
  }
}